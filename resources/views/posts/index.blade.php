@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				
			<h2>All posts <a href="{{route('posts.create')}}" class="btn btn-success pull-right">Create</a></h2>
		@foreach($posts as $post)
			<div class="single-post" style="margin-bottom: 20px">
				<div class="row">
					<div class="col-sm-5">
						<img class="img-responsive" src="{{asset($post->image)}}" alt="">
					</div>	
					<div class="col-sm-7">
						<h4>{{$post->post_category->name}}</h4>
						<h3>{{$post->title}}</h3>
						<p>{{$post->description}}</p>

						<p><span>Created By : {{$post->user->name}}</span> <span class="pull-right">Created at: {{$post->created_at->toFormattedDateString()}}</span></p>
					</div>	
					
				</div>
			</div>
			@endforeach

			</div>		
		</div>
	</div>
@endsection